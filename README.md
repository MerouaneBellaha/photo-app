
![](/appArchitecture.png)

This Single view photo has been made as part of my iOs developper course from OpenClassrooms.

Here are the constraints:

  1: Follow the AdobeXd design file. 
  
  2: Usable in portrait and landscape.  
  
  3: User can choose between 3 different layout. Add an image from his photo library in the chosen layout.  
     User can share his layout.
  
What have been learned:

  1: UIImagePickerController  
     UISwipeGestureRecognizer  
     UIActivityViewController  
     NotificationCenter 
     
  2: Auto-layout, responsiveness

